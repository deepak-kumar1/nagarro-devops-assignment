﻿using Data.Table.RelationshipDomain.Mapper;
using Data.Table.RelationshipDomain.Repository;
using Data.Table.RelationshipShared.Infrastructure.StudentInterface;
using Data.Table.RelationshipShared.Infrastructure.TeacherInterface;
using Data.Table.RelationshipShared.Infrastructure.UserIntreface;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Table.RelationshipDomain.Configuration
{
    public static class ExtensionUnitOfwork
    {
        public static IServiceCollection RegisterDataContext(this IServiceCollection services, string connectionString)
        {
            services.AddAutoMapper(typeof(MappingData));
            services.AddScoped<IUserRepository,UserRepository>();
            services.AddScoped<ITeacherRepository,TeacherRepository>();
            services.AddScoped<IStudentRepository,StudentRepository>();
            services.AddDbContext<ApplicationDb>(options =>
                options.UseSqlServer(connectionString));

            return services;
        }
    }
}
