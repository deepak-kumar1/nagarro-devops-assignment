﻿using AutoMapper;
using Data.Table.RelationshipDomain.Entites;
using Data.Table.RelationshipShared.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Data.Table.RelationshipDomain.Mapper
{
    public class MappingData:Profile
    {
        public MappingData() {
            CreateMap<UserDTO, User>().ReverseMap();
            CreateMap<CourseDTO, Course>().ReverseMap();
            CreateMap<TeacherDTO, Teacher>().ReverseMap();
            CreateMap<UserDTO,User>().ReverseMap();
            CreateMap<LoginDTO,User>().ReverseMap();
            CreateMap<CourseEnrollmentDTO, CourseEnrollment>().ReverseMap();
        }
    }
}
