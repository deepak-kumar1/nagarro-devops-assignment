﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Table.RelationshipDomain.Entites
{
    public class Teacher
    {
        public int Id { get; set; }
        public virtual User? User { get; set; }

        public string? Specialization { get; set; }

        public virtual ICollection<Course>? Courses { get; set; }

    }
}
