﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Table.RelationshipDomain.Entites
{
    public class Course
    {
        public int Id { get; set; }
        public string? Coursename { get; set; }
        public int TeacherId { get; set; } // Foreign key property
        public string? Image { get; set; }
        public virtual Teacher? Teacher { get; set; }

        // Navigation properties
        public virtual ICollection<CourseEnrollment>? CourseEnrollments { get; set; }


    }
}
