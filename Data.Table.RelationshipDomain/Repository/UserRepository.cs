﻿using AutoMapper;
using Data.Table.RelationshipDomain.Entites;
using Data.Table.RelationshipShared.DTOs;
using Data.Table.RelationshipShared.Infrastructure.UserIntreface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Data.Table.RelationshipDomain.Repository
{
    public class UserRepository:IUserRepository
    {
        private readonly ApplicationDb _db;
        private readonly IMapper _mapper;
        public UserRepository(ApplicationDb db,IMapper mapper)
        {

            _db = db;
            _mapper=mapper;

        }
        private static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public async Task<UserDTO> RegisterUser(UserDTO userDTO)
        {
            var userInfo = _mapper.Map<UserDTO, User>(userDTO);
            User user=new User()
            {
                Name = userInfo.Name,
                Email = userInfo.Email,
                Specialization = userInfo.Specialization,
                Password = Base64Encode(userInfo.Password),
                //Role=UserType.Teacher.ToString(),
                Role=userInfo.Role,
            };
           var res= await _db.Users.AddAsync(user);
            await _db.SaveChangesAsync();
            if (res != null)
            {
                var newUser = _db.Users.FirstOrDefault(u => u.Email == userDTO.Email);
                //Storing Teacher data
                Teacher teacher = new Teacher()
                {
                    User = newUser,
                    Specialization=userDTO.Specialization,
                };
                await _db.Teachers.AddAsync(teacher);
                await _db.SaveChangesAsync();
                //Storing Data in address table
            }
            var response=_mapper.Map<User, UserDTO>(userInfo);
            return response;

        }
        public async Task<UserDTO> GetUserByEmail(LoginDTO loginDTO)
        {
            var userInfo = _mapper.Map<User>(loginDTO);
            var res = await _db.Users.FirstOrDefaultAsync(u => u.Password == Base64Encode(userInfo.Password) && u.Email==userInfo.Email);
            return _mapper.Map<User, UserDTO>(res);
        }
    }
}
