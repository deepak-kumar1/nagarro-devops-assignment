﻿using AutoMapper;
using Data.Table.RelationshipDomain.Entites;
using Data.Table.RelationshipShared.DTOs;
using Data.Table.RelationshipShared.Infrastructure.StudentInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Table.RelationshipDomain.Repository
{
    public class StudentRepository:IStudentRepository
    {
        private readonly ApplicationDb _db;
        private readonly IMapper _mapper;
        public StudentRepository(ApplicationDb db, IMapper mapper)
        {

            _db = db;
            _mapper = mapper;

        }
        public async Task<String> GetEnrolled(CourseEnrollmentDTO courseEnrollmentDTO)
        {
            var courseInfo = _mapper.Map<CourseEnrollmentDTO, CourseEnrollment>(courseEnrollmentDTO);
            var userId= _db.Users.FirstOrDefault(u => u.Id == courseInfo.UserId);
            var courseId = _db.Courses.FirstOrDefault(u => u.Id == courseInfo.CourseId);
            if (userId != null || courseId != null)
            {
                
                CourseEnrollment courseEnrollment = new CourseEnrollment()
                {
                    UserId=userId.Id,
                    CourseId=courseId.Id
                };
                await _db.CourseEnrollments.AddAsync(courseEnrollment);
                await _db.SaveChangesAsync();
            }
            return "You have enrolled in a Course";
        }
    }
}
