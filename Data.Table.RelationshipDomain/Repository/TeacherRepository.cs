﻿using AutoMapper;
using Data.Table.RelationshipDomain.Entites;
using Data.Table.RelationshipShared.DTOs;
using Data.Table.RelationshipShared.Infrastructure.TeacherInterface;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;

namespace Data.Table.RelationshipDomain.Repository
{
    public class TeacherRepository : ITeacherRepository
    {
        private readonly ApplicationDb _db;
        private readonly IMapper _mapper;

        public TeacherRepository(ApplicationDb db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public async Task<string> AddCourse(CourseDTO courseDTO)
        {
            var courseInfo = _mapper.Map<CourseDTO, Course>(courseDTO);
            var teacher = await _db.Teachers.FirstOrDefaultAsync(u => u.Id == courseInfo.TeacherId);

            if (teacher != null)
            {
                Course course = new Course()
                {
                    Coursename = courseInfo.Coursename,
                    TeacherId = teacher.Id,
                    Image = courseInfo.Image,
                };
                await _db.Courses.AddAsync(course);
                await _db.SaveChangesAsync();
            }
            return "Course Added Successfully!";
        }

        public async Task<IEnumerable<CourseDTO>> GetAllCourse()
        {
            var courses = await _db.Courses.ToListAsync();
            return _mapper.Map<IEnumerable<Course>, IEnumerable<CourseDTO>>(courses);
        }

       public async Task<String> CourseUpdate(CourseDTO courseDTO)
        {
            var courseInfo = _mapper.Map<CourseDTO, Course>(courseDTO);
            var IsCourse = await _db.Courses.FirstOrDefaultAsync(c => c.Id == courseInfo.Id);
            var IsTeacher = await _db.Teachers.FirstOrDefaultAsync(u => u.Id == courseInfo.TeacherId);
            if (IsCourse != null && IsTeacher != null)
            {
                IsCourse.Coursename = courseInfo.Coursename;
                IsCourse.Image = courseInfo.Image;
                await _db.SaveChangesAsync();
            }
            return "Course Updated Successfully!";
        }
        public async Task<String> DeleteCourseAsync(int id)
        {
            var res=await _db.Courses.FirstOrDefaultAsync(c=>c.Id==id);
            if (res != null)
            {
                var response = _db.Courses.Remove(res);
                await _db.SaveChangesAsync();
            }
            return "Course Deleted Successfully!";
        }

    }
}
