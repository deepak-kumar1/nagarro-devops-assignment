﻿using Data.Table.RelationshipShared.DTOs;
using Data.Table.RelationshipShared.Infrastructure.JwtHandler;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Data.Table.RelationshipSecurity.JWT
{
    public class JwtTokenGenerator : IJwtTokenGenerator
    {
        private readonly JwtOptions _jwtOptions;
        public JwtTokenGenerator(IOptions<JwtOptions> jwtOptions)
        {
            _jwtOptions = jwtOptions.Value;
        }
        public string GenerateToken(UserDTO userApplicationDTO, string role)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtOptions.Secret);
            //var value = _jwtOptions.Audience;
            var claimList = new List<Claim>
                {
                    new Claim(JwtRegisteredClaimNames.Email,userApplicationDTO.Email),
                    new Claim(JwtRegisteredClaimNames.Name,userApplicationDTO.Password),
                     new Claim(JwtRegisteredClaimNames.Name,userApplicationDTO.Name)

                };
            // claimList.AddRange(roles.Select(role => new Claim(ClaimTypes.Role, role)));
            claimList.Add(new Claim(ClaimTypes.Role, role));
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Audience = _jwtOptions.Audience,
                Issuer = _jwtOptions.Issuer,
                Subject = new ClaimsIdentity(claimList),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
