﻿using Data.Table.RelationshipShared.DTOs;
using Data.Table.RelationshipShared.Infrastructure.TeacherInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Table.RelationshipBusiness.AppService.UserService
{
    public class TeacherService:ITeacherService
    {
        private readonly ITeacherRepository _repository;
        public TeacherService(ITeacherRepository teacherRepository)
        {
            _repository = teacherRepository;
        }
        public async Task<string> CourseAdd(CourseDTO courseDTO)
        {
            var res=await _repository.AddCourse(courseDTO);
            return res;
        }

        public async Task<IEnumerable<CourseDTO>> GetAllCourse()
        {
            var res = await _repository.GetAllCourse();
            return res;
        }
        public async Task<string> CourseUpdate(CourseDTO courseDTO)
        {
            var res = await _repository.CourseUpdate(courseDTO);
            return res;
        }

        public async Task<String> DeleteCourseAsync(int id)
        {
            var res = await _repository.DeleteCourseAsync(id);
            return res;
        }

    }
}
