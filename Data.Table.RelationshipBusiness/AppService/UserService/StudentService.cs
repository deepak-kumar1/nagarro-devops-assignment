﻿using Data.Table.RelationshipShared.DTOs;
using Data.Table.RelationshipShared.Infrastructure.StudentInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Table.RelationshipBusiness.AppService.UserService
{
    public class StudentService:IStudentService
    {
        private readonly IStudentRepository _studentRepository;
        public StudentService(IStudentRepository studentRepository)
        {

            _studentRepository = studentRepository;

        }
        public async Task<String> GetEnrollMent(CourseEnrollmentDTO courseEnrollmentDTO)
        {
            var res = await _studentRepository.GetEnrolled(courseEnrollmentDTO);
            return res;
        }
    }
}
