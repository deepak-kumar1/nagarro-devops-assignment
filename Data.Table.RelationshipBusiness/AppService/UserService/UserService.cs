﻿using Data.Table.RelationshipShared.DTOs;
using Data.Table.RelationshipShared.Infrastructure.UserIntreface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Table.RelationshipBusiness.AppService.UserService
{
    public class UserService:IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {

            _userRepository = userRepository;

        }

        public async Task<UserDTO> RegisterUser(UserDTO userDTO)
        {
            var response=await _userRepository.RegisterUser(userDTO);
            return response;
        }
        public async Task<UserDTO> GetUser(LoginDTO loginDTO)
        {
            var res = await _userRepository.GetUserByEmail(loginDTO);
            return res;
        }
    }
}
