﻿using Microsoft.Extensions.DependencyInjection;
using Data.Table.RelationshipDomain.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Table.RelationshipShared.Infrastructure.UserIntreface;
using Data.Table.RelationshipBusiness.AppService.UserService;
using Data.Table.RelationshipShared.Infrastructure.TeacherInterface;
using Data.Table.RelationshipShared.Infrastructure.StudentInterface;

namespace Data.Table.RelationshipBusiness.Configuration
{
    public static class ExtensionUnitOfWork
    {

        public static IServiceCollection RegisterServices(this IServiceCollection services, string connectionString)
        {

            //DbContext and repository configurations of Data Layer
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITeacherService, TeacherService>();
            services.AddScoped<IStudentService, StudentService>();
            services.RegisterDataContext(connectionString);
            return services;
        }
    }
}
