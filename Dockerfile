
FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
ENV ASPNETCORE_ENVIRONMENT="Development"
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["Data.Table.RelationshipWeb/Data.Table.RelationshipWeb.csproj", "Data.Table.RelationshipWeb/"]
COPY ["Data.Table.RelationshipBusiness/Data.Table.RelationshipBusiness.csproj", "Data.Table.RelationshipBusiness/"]
COPY ["Data.Table.RelationshipDomain/Data.Table.RelationshipDomain.csproj", "Data.Table.RelationshipDomain/"]
COPY ["Data.Table.RelationshipShared/Data.Table.RelationshipShared.csproj", "Data.Table.RelationshipShared/"]
COPY ["Data.Table.RelationshipSecurity/Data.Table.RelationshipSecurity.csproj", "Data.Table.RelationshipSecurity/"]
RUN dotnet restore "Data.Table.RelationshipWeb/Data.Table.RelationshipWeb.csproj"
COPY . .
WORKDIR "/src/Data.Table.RelationshipWeb"
RUN dotnet build "Data.Table.RelationshipWeb.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Data.Table.RelationshipWeb.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Data.Table.RelationshipWeb.dll","--environment=Development"]