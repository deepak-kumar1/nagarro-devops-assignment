using Data.Table.RelationshipBusiness.Configuration;
using Data.Table.RelationshipSecurity.Configuration;
using Data.Table.RelationshipSecurity.JWT;
using Data.Table.RelationshipWeb.DecodeToken;
using Microsoft.EntityFrameworkCore;


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
// Add services to the container.
var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
if (connectionString != null)
{
    builder.Services.RegisterServices(connectionString);
}
var settingsSection = builder.Configuration.GetSection("JwtToken");
var jwtOptions = builder.Configuration.GetSection("ApiSettings:JwtOptions").Get<JwtOptions>();
if (jwtOptions != null)
{
    builder.Services.JwtConfigured(jwtOptions, settingsSection);
}

builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(
        policy =>
        {
            policy.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
        });
});
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseCors();
app.UseAuthorization();


app.MapControllers();

app.Run();
     