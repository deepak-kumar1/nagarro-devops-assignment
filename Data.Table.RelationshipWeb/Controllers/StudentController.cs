﻿using Data.Table.RelationshipShared.DTOs;
using Data.Table.RelationshipShared.Infrastructure.StudentInterface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Data.Table.RelationshipWeb.Controllers
{
    [Route("student")]
    public class StudentController : ControllerBase
    {
        private readonly IStudentService _studentService;
        public StudentController(IStudentService studentService)
        {
            _studentService = studentService;
        }
        [HttpPost]
        [Route("course/student")]
        [Authorize("User")]
        public async Task<IActionResult> EnrollmentCourse([FromBody] CourseEnrollmentDTO courseEnrollmentDTO)
        {
            var res = await _studentService.GetEnrollMent(courseEnrollmentDTO);
            return Ok(res);
        }
    }
}
