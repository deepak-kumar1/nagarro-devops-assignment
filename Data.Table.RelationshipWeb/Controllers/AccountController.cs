﻿using Azure;
using Data.Table.RelationshipShared.DTOs;
using Data.Table.RelationshipShared.Infrastructure.JwtHandler;
using Data.Table.RelationshipShared.Infrastructure.UserIntreface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Data.Table.RelationshipWeb.Controllers
{
    [Route("account")]
    public class AccountController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly ResponseDTO _responseDTO;
        private readonly IJwtTokenGenerator _jwtTokenGenerator;
        public AccountController(IUserService userService,IJwtTokenGenerator jwtTokenGenerator)
        {
            _userService = userService;
            _responseDTO = new ResponseDTO();
            _jwtTokenGenerator = jwtTokenGenerator;
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> RegisterUser([FromBody] UserDTO userDTO)
        {
            var response = await _userService.RegisterUser(userDTO);
            if (response == null)
            {
                return NotFound("Something Went Wrong!");
            }
           _responseDTO.IsSuccess = true;
            _responseDTO.Message = "Register SuccessFully!";
            _responseDTO.Token = "";
            return Ok(_responseDTO);

        }
        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginDTO loginDTO)
        {
            var user = await _userService.GetUser(loginDTO);
            if (user == null)
            {
                return Unauthorized();
            }
            // Generate JWT token
            var token = _jwtTokenGenerator.GenerateToken(user, user.Role);
            _responseDTO.IsSuccess = true;
            _responseDTO.Message = "login SuccessFully";
            _responseDTO.Token = token;
            _responseDTO.Information = user;
            return Ok(_responseDTO);

        }
        
    }
}
