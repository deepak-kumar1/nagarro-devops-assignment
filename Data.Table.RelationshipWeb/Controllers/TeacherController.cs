﻿using Data.Table.RelationshipShared.DTOs;
using Data.Table.RelationshipShared.Infrastructure.TeacherInterface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Data.Table.RelationshipWeb.Controllers
{
    [Route("teacher")]
    public class TeacherController : ControllerBase
    {
        private readonly ITeacherService _teacherService;
        private readonly ResponseDTO _responseDTO;
        public TeacherController(ITeacherService teacherService)
        {
            _teacherService = teacherService;
            _responseDTO= new ResponseDTO();
        }

        [HttpPost]
        [Route("course/add")]
       // [Authorize("Teacher")]
        public async Task<IActionResult> CourseAdd([FromBody] CourseDTO courseDTO)
        {
            var res = await _teacherService.CourseAdd(courseDTO);
            if(res==null)
            {
                return BadRequest("Course Not Added!");
            }
            _responseDTO.IsSuccess = true;
            _responseDTO.Message = res;
            return Ok(_responseDTO);
        }
        [HttpGet]
        [Route("fetch/allCourse")]
        [Authorize(Roles="Teacher")]
        public async Task<IActionResult> GetAllCourse()
        {
            var res=await _teacherService.GetAllCourse();
            _responseDTO.IsSuccess = true;
            _responseDTO.Information = res;
            return Ok(_responseDTO);
        }
        [HttpPut]
        [Route("update/course")]
        [Authorize(Roles="Teacher")]
        public async Task<IActionResult> UpdatedCourse([FromBody] CourseDTO courseDTO)
        {
            var res = await _teacherService.CourseUpdate(courseDTO);
            if (res == null)
            {
                return BadRequest("Course Not Updated!");
            }
            _responseDTO.IsSuccess = true;
            _responseDTO.Message = res;
            return Ok(_responseDTO);
        }
        [HttpDelete]
        [Route("delete/course/{id}")]
        [Authorize("Teacher")]
        public async Task<IActionResult> DeleteCourse(int id)
        {
            var res = await _teacherService.DeleteCourseAsync(id);
            if( res == null)
            {
                return NotFound("Something Went Wrong while Deleting Course!");
            }
            _responseDTO.IsSuccess = true;
            _responseDTO.Message= res;
            return Ok(_responseDTO);
        }
    }
}
