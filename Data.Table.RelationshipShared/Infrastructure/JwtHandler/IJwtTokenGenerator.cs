﻿using Data.Table.RelationshipShared.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Table.RelationshipShared.Infrastructure.JwtHandler
{
    public interface IJwtTokenGenerator
    {
        string GenerateToken(UserDTO userApplicationDTO, string roles);
    }
}
