﻿using Data.Table.RelationshipShared.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Table.RelationshipShared.Infrastructure.TeacherInterface
{
    public interface ITeacherRepository
    {
        public Task<string> AddCourse(CourseDTO courseDTO);
        public Task<IEnumerable<CourseDTO>> GetAllCourse();
        public Task<String> CourseUpdate(CourseDTO courseDTO);

        public Task<String> DeleteCourseAsync(int id);
    }
}
