﻿using Data.Table.RelationshipShared.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Table.RelationshipShared.Infrastructure.TeacherInterface
{
    public interface ITeacherService
    {
        public Task<string> CourseAdd(CourseDTO courseDTO);
        public Task<IEnumerable<CourseDTO>> GetAllCourse();
        public Task<string> CourseUpdate(CourseDTO courseDTO);

        public Task<String> DeleteCourseAsync(int id);
    }
}
