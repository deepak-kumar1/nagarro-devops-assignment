﻿using Data.Table.RelationshipShared.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Table.RelationshipShared.Infrastructure.UserIntreface
{
    public interface IUserRepository
    {
        public Task<UserDTO> RegisterUser(UserDTO userDTO);
        public Task<UserDTO> GetUserByEmail(LoginDTO loginDTO);
    }
}
