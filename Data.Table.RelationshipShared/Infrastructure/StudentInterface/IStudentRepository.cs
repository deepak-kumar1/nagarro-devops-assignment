﻿using Data.Table.RelationshipShared.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Table.RelationshipShared.Infrastructure.StudentInterface
{
    public interface IStudentRepository
    {
        public Task<string> GetEnrolled(CourseEnrollmentDTO courseEnrollmentDTO);
    }
}
