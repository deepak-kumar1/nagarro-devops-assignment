﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Table.RelationshipShared.DTOs
{
    public class ResponseDTO
    {
        public bool IsSuccess { get; set; }

        public string? Message { get; set; }

        public string? Token { get; set; }
        public Object? Information { get; set; }    
    }
}
