﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Table.RelationshipShared.DTOs
{
    public class TeacherDTO
    {
        public int Id { get; set; }
        public virtual UserDTO? User { get; set; }

        public string? Specialization { get; set; }

        //public virtual ICollection<CourseDTO>? Courses { get; set; }
    }
}
