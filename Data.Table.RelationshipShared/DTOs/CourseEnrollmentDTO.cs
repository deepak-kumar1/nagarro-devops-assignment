﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Table.RelationshipShared.DTOs
{
    public class CourseEnrollmentDTO
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int CourseId { get; set; }
        //public virtual UserDTO? User { get; set; }
       // public virtual CourseDTO? Course { get; set; }
    }
}
