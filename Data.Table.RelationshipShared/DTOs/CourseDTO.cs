﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Table.RelationshipShared.DTOs
{
    public class CourseDTO
    {
        public int Id { get; set; }
        public string? Coursename { get; set; }
        public int TeacherId { get; set; } // Foreign key property
        public string? Image { get; set; }
        //public virtual TeacherDTO? Teacher { get; set; }

        // Navigation properties
       // public virtual ICollection<CourseEnrollmentDTO>? CourseEnrollments { get; set; }

    }
}
